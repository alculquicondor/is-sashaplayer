package com.sasha.player;

/**
 * Created by alculquicondor on 02/10/14.
 */
public class User implements Followed {
    private String username;
    private String email;
    private String passwordHash;
    private Role role;
    private java.util.List<PlayList> ownedPlaylists;
    private java.util.List<Followed> following;
    private java.util.List<Song> likes;

}
