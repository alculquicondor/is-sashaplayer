package com.sasha.player;

/**
 * Created by alculquicondor on 02/10/14.
 */
public class Song {
    private String title;
    private String youtubeId;
    private java.util.Vector<Author> authors;
    private Album album;
    private String lyrics;
}
